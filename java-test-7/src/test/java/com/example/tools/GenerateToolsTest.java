package com.example.tools;

import org.junit.Test;

import java.io.File;

public class GenerateToolsTest {
    /**
     * 将官方文档转换成标准Asciidoc文档
     */
    @Test
    public void convertCustom() {
        String version = "7.12.0";
        String classesPath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
        File rootFile = new File(classesPath).getParentFile().getParentFile().getParentFile();
        String folderString = rootFile.getPath() + File.separator + "asciidoc" + File.separator + "cn" + File.separator + version;
        File file = new File(folderString);
//        GenerateTools.resolveJava(file);
        GenerateTools.resolveAdoc(file);
    }
}