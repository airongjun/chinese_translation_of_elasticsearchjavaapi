package com.example.tools;

import com.example.tools.generate.ConvertAdoc;
import com.example.tools.generate.ConvertJava;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * 官方文档转换工具类
 */
public class GenerateTools {

    /**
     * 解析JAVA文件
     *
     * @param file 根目录
     */
    public static void resolveJava(File file) {
        File folder = new File(file.getPath() + File.separator + "client");
        List<File> filterFile = filterFile(folder, "java");
        ConvertJava.convert(filterFile);
    }

    /**
     * 解析文档文件
     *
     * @param file 根目录
     */
    public static void resolveAdoc(File file) {
        File folder = new File(file.getPath() + File.separator + "java-rest");
        List<File> filterFile = filterFile(folder, "asciidoc");
        List<File> filterFile1 = filterFile(folder, "adoc");
        filterFile.addAll(filterFile1);
        ConvertAdoc.convert(filterFile);
    }

    /**
     * 根据拓展名检索出所有文件以及文件夹下的子文件
     *
     * @param file      文件或文件夹
     * @param extension 拓展名
     * @return 匹配拓展名的文件列表
     */
    private static List<File> filterFile(File file, String extension) {
        List<File> result = new ArrayList<>();
        if (!file.exists()) {
            return result;
        }
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            if (files != null && files.length != 0) {
                for (File file1 : files) {
                    List<File> files1 = filterFile(file1, extension);
                    result.addAll(files1);
                }
            }
        } else {
            if (file.getName().endsWith("." + extension)) {
                result.add(file);
            }
        }
        return result;
    }


}
