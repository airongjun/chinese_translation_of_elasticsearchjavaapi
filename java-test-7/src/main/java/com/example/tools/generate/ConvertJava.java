package com.example.tools.generate;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.ObjectUtil;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ConvertJava {
    public static void convert(List<File> fileList) {
        if (ObjectUtil.isEmpty(fileList)) {
            return;
        }
        for (File file : fileList) {
            String content = FileUtil.readString(file, StandardCharsets.UTF_8);

            String newContent = convert1(content);
            newContent = convert2(newContent);

            if (newContent != null && !newContent.equals(content)) {
                FileUtil.writeString(newContent, file, StandardCharsets.UTF_8);
            }
        }
    }

    private static String convert2(String content) {
        String regex2 = "(?!\\n)^(\\s*?)//tag::(.*)\\[]([\\s\\S]*?)//end::(.*)\\[]$";
        Matcher matcher2 = Pattern.compile(regex2, Pattern.MULTILINE).matcher(content);
        if (matcher2.find()) {
            return matcher2.replaceAll(s -> {
                String str = s.group(0);
                str = Matcher.quoteReplacement(str);
                int tabCount = str.split("//tag")[0].length();
                StringBuilder sb = new StringBuilder();
                String[] split = str.split("\n");
                for (String s1 : split) {
                    if (s1.length() >= tabCount) {
                        sb.append(s1.substring(tabCount));
                    }
                    sb.append("\n");
                }
                sb.setLength(sb.length() - 1);
                return sb.toString();
            });
        } else {
            return content;
        }
    }

    private static String convert1(String content) {
        String regex1 = "^([\\S\\s]*?)//(\\s*?)(tag|end)::(.*?)(?<!\\[])(\\s*?)$";
        Matcher matcher1 = Pattern.compile(regex1, Pattern.MULTILINE).matcher(content);
        if (matcher1.find()) {
            return matcher1.replaceAll("$1//$3::$4[]");
        } else {
            return content;
        }
    }

}
