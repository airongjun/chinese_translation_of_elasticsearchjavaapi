#!/usr/bin/env bash

DOC_FOLDER=/Users/wangbin/src/docs/chinese_translation_of_elasticsearchjavaapi/asciidoc/cn/7.16.0
OUTPUT_FOLDER=/Users/wangbin/src/docs/chinese_translation_of_elasticsearchjavaapi/docs/7.16.0
asciidoctor \
${DOC_FOLDER}/docs/index.asciidoc \
-o ${OUTPUT_FOLDER}/index.html \
-a docdir=${DOC_FOLDER}/docs \
-a stylesheet=${DOC_FOLDER}/../../base/css.css \
-a elasticsearch-root=${DOC_FOLDER}/elasticsearch-root
