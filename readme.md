# ElasticSearch-Java API      

更新日志：

2022-06-08

> 准备支持antora架构，[预览地址](https://consolelog.gitee.io/docs-es-java/)。再完成后会覆盖当前的地址。

2022-02-24

> 7.16.0版本初稿完成， [在线地址](https://consolelog.gitee.io/chinese_translation_of_elasticsearchjavaapi/7.16.0/) 。
接下来的计划是8.x版本。

2021-12-14

> 7.16.0版本正在施工， [在线地址](https://consolelog.gitee.io/chinese_translation_of_elasticsearchjavaapi/7.16.0/) 。
之前的7.12版本由于没时间搞，所以GG了，这次希望能坚持下来。

2019-05-28

> 6.8.0版本初稿完成，[在线地址](https://consolelog.gitee.io/chinese_translation_of_elasticsearchjavaapi/6.8.0/)

2019-05-22

> 6.8.0版本正在施工,[在线地址](https://consolelog.gitee.io/chinese_translation_of_elasticsearchjavaapi/)

> 7.x在计划中，欢迎小伙伴们参与贡献

## 6.2.2版本目录
- [前言](markdown/1Preface/readme.md)
- [Javadoc](markdown/2JavaDoc/readme.md)
- [Maven仓库](markdown/3MavenRepository/readme.md)
- [Client](markdown/4Client/readme.md)
- [Document接口](markdown/5DocumentAPIs/readme.md)
- [Search接口](markdown/6SearchAPI/readme.md)
- [聚合](markdown/7Aggregations/readme.md)
- [领域查询](markdown/8QueryDSL/readme.md)
- [管理员接口](markdown/9JavaAPIAdministration/readme.md)


- [《Elasticsearch: 权威指南》中文版](https://www.elastic.co/guide/cn/elasticsearch/guide/current/index.html)
- [名词解释(对比关系型数据库)](markdown/0Remark/words.md)
- [贡献说明](markdown/0Remark/Contribution.md)

> 原文[地址](https://www.elastic.co/guide/en/elasticsearch/reference/6.2/index.html)

> osc[地址](https://gitee.com/consolelog/chinese_translation_of_elasticsearchjavaapi)

> 本文旨在帮助中文用户学习ES，转载请注明出处。

> 欢迎大家踊跃提交issue/pull request。联系方式：codeforfun@foxmail.com




